﻿namespace Complex.Models
{
    public class ImageSettings
    {
        public FunctionType FunctionType;
        public double Scale;
        public DoublePosition Offset;
        public IntSize Size;
    }
}   