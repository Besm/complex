﻿namespace Complex.Models
{
    public class RequestTask
    {
        public IntSize MaxChunkSize;
        public ImageSettings ImageSettings;
    }
}
