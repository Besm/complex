﻿using System;

namespace Complex.Models
{
    public class GroupingTask
    {
        public Guid ImageId;
        public int ChunkCount;
        public GeneratedChunk Chunk;
    }
}
