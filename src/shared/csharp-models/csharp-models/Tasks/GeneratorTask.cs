﻿using System;
using System.Collections.Generic;

namespace Complex.Models
{
    public class GeneratorTask
    {
        public Guid ImageId;
        public int ChunkId;
        public int ChunkCount;
        public IntRectangle Rect;
        public ImageSettings ImageSettings;
    }
}
