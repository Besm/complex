﻿namespace Complex.Models
{
    public struct IntPosition
    {
        public int X;
        public int Y;

        public IntPosition(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static IntPosition operator +(IntPosition position, IntSize size)
        {
            return new IntPosition(position.X + size.Width, position.Y + size.Height);
        }
    }
}
