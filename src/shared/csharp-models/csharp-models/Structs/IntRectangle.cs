﻿namespace Complex.Models
{
    public struct IntRectangle
    {
        public IntPosition Position;
        public IntSize Size;
    }
}
