﻿namespace Complex.Models
{
    public struct DoublePosition
    {
        public double X;
        public double Y;

        public DoublePosition(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}
