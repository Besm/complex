﻿using System.Collections.Generic;
using System.Text;

namespace Complex.Models
{
    public class GeneratedChunk
    {
        public IntRectangle Rect;
        public string CompressedBytes;
    }
}
