﻿using System;

namespace Complex.Models
{
    public class Image
    {
        public Guid Id;
        public string CompressedBase64RGBABytes;
    }
}