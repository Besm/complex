﻿using System;

namespace Complex.Models
{
    public class Chunk
    {
        public int Id { get; set; }
        public Guid ImageGuid { get; set; }
        public int ChunkCount { get; set; }
        public string CompressedBytes { get; set; }

        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}