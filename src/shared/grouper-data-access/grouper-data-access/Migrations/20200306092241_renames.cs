﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Complex.Migrations
{
    public partial class renames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Chunk_Images_ImageId",
                table: "Chunk");

            migrationBuilder.DropTable(
                name: "Images");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Chunk",
                table: "Chunk");

            migrationBuilder.DropIndex(
                name: "IX_Chunk_ImageId",
                table: "Chunk");

            migrationBuilder.DropColumn(
                name: "ImageId",
                table: "Chunk");

            migrationBuilder.RenameTable(
                name: "Chunk",
                newName: "Chunks");

            migrationBuilder.AddColumn<int>(
                name: "ChunkCount",
                table: "Chunks",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "CompressedBytes",
                table: "Chunks",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Height",
                table: "Chunks",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "ImageGuid",
                table: "Chunks",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "Width",
                table: "Chunks",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "X",
                table: "Chunks",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Y",
                table: "Chunks",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Chunks",
                table: "Chunks",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Chunks",
                table: "Chunks");

            migrationBuilder.DropColumn(
                name: "ChunkCount",
                table: "Chunks");

            migrationBuilder.DropColumn(
                name: "CompressedBytes",
                table: "Chunks");

            migrationBuilder.DropColumn(
                name: "Height",
                table: "Chunks");

            migrationBuilder.DropColumn(
                name: "ImageGuid",
                table: "Chunks");

            migrationBuilder.DropColumn(
                name: "Width",
                table: "Chunks");

            migrationBuilder.DropColumn(
                name: "X",
                table: "Chunks");

            migrationBuilder.DropColumn(
                name: "Y",
                table: "Chunks");

            migrationBuilder.RenameTable(
                name: "Chunks",
                newName: "Chunk");

            migrationBuilder.AddColumn<int>(
                name: "ImageId",
                table: "Chunk",
                type: "int",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Chunk",
                table: "Chunk",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Images",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Images", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Chunk_ImageId",
                table: "Chunk",
                column: "ImageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Chunk_Images_ImageId",
                table: "Chunk",
                column: "ImageId",
                principalTable: "Images",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
