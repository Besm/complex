﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace Complex.Communication
{
    public class MessagePublisher
    {
        private readonly IModel channel;
        private readonly IConnection connection;
        private readonly IBasicProperties properties;
        private string queueName;

        public MessagePublisher(Uri endpoint, string queueName)
        {
            var factory = new ConnectionFactory
            {
                Endpoint = new AmqpTcpEndpoint(endpoint)
            };
            this.queueName = queueName;
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            channel.QueueDeclare(queueName, false,false,false);
            channel.QueueBind(queueName, "amq.direct", queueName);
            properties = channel.CreateBasicProperties();
        }

        public void Publish(object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            RawPublish(Encoding.UTF8.GetBytes(json));
        }

        private void RawPublish(byte[] bytes)
        {
            channel.BasicPublish("amq.direct", queueName, properties, bytes);
        }
    }
}
