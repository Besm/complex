﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Threading;

namespace Complex.Communication
{
    public class MessageConsumer
    {
        private IModel channel;
        private IConnection connection;
        private EventingBasicConsumer consumer;
        private ManualResetEvent resetEvent = new ManualResetEvent(false);
        private string QueueName;
        public event EventHandler<DeliveryEventArgs> OnNewMessage;
        private ConnectionFactory ConnectionFactory;
        public MessageConsumer(Uri endpoint, string queueName)
        {
            ConnectionFactory = new ConnectionFactory
            {
                Endpoint = new AmqpTcpEndpoint(endpoint)
            };
            QueueName = queueName;
            connection = ConnectionFactory.CreateConnection();
            channel = connection.CreateModel();
            channel.QueueDeclare(QueueName, false, false, false);
            channel.QueueBind(QueueName, "amq.direct", QueueName);
            consumer = new EventingBasicConsumer(channel);
            consumer.Received += Consumer_Received;
        }

        public void StartRecieiving()
        {
            channel.BasicConsume(QueueName, false, consumer);
        }

        public void WaitForever()
        {
            while (true)
            {
                Thread.Sleep(1);
                resetEvent.WaitOne();
            }
        }

        private void Consumer_Received(object sender, BasicDeliverEventArgs e)
        {
            OnNewMessage.Invoke(sender, new DeliveryEventArgs(e));
            channel.BasicAck(e.DeliveryTag, false);
            resetEvent.Set();
        }
    }
}
