﻿using System;

namespace Complex.Communication
{
    /// <summary>
    /// A worker that always consumes from one specific queue and always produces to one specific queue.
    /// </summary>
    public abstract class ForwardQueueWorker
    {
        private MessageConsumer MessageConsumer { get; set; }
        private MessagePublisher MessagePublisher { get; set; }

        public ForwardQueueWorker(Uri ampqEndpointUri, string consumingQueue, string producingQueue)
        {
            MessageConsumer = new MessageConsumer(ampqEndpointUri, consumingQueue);
            MessagePublisher = new MessagePublisher(ampqEndpointUri, producingQueue);
        }

        public virtual void Start()
        {
            MessageConsumer.OnNewMessage += MessageConsumer_OnNewMessage;
            MessageConsumer.StartRecieiving();
            MessageConsumer.WaitForever();
        }

        protected abstract void ProcessMessage(DeliveryEventArgs deliveryEventArgs);

        protected void Publish(object obj)
        {
            MessagePublisher.Publish(obj);
        }

        private void MessageConsumer_OnNewMessage(object sender, DeliveryEventArgs e)
        {
            ProcessMessage(e);
        }
    }
}
