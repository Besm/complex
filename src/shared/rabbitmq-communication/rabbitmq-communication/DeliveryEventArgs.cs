﻿using Newtonsoft.Json;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace Complex.Communication
{
    public class DeliveryEventArgs : EventArgs
    {
        private byte[] bytes;

        public DeliveryEventArgs(BasicDeliverEventArgs e)
        {
            bytes = e.Body;
        }

        public T GetValue<T>()
        {
            var json = Encoding.UTF8.GetString(bytes);
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
