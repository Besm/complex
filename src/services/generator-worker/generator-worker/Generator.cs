﻿using Complex.Communication;
using Complex.Models;
using RabbitMQ.Client;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Generator
{
    public class Generator : ForwardQueueWorker
    {
        private ComplexSetGenerator ComplexSetGenerator { get; set; } = new ComplexSetGenerator();

        public Generator(Uri ampqEndpointUri) : base(ampqEndpointUri, "generator/requests", "grouping/requests")
        {
            Console.WriteLine("url=" + ampqEndpointUri.AbsoluteUri);
        }

        public override void Start()
        {
            Console.WriteLine("generator-worker started");
            base.Start();
        }

        protected override void ProcessMessage(DeliveryEventArgs deliveryEventArgs)
        {
            var task = deliveryEventArgs.GetValue<GeneratorTask>();

            var chunk = ComplexSetGenerator.Generate(task);
            var groupingTask = new GroupingTask()
            {
                Chunk = chunk,
                ChunkCount = task.ChunkCount,
                ImageId = task.ImageId
            };

            Publish(groupingTask);
        }
    }
}
