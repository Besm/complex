﻿using System;

namespace Generator
{
    class Program
    {
        private const string connectionurl = "amqp://rabbitmq.complex-18564455-production.svc.cluster.local";

        static void Main(string[] args)
        {
            Console.WriteLine(connectionurl);
            Generator generator = new Generator(new Uri(connectionurl));

            generator.Start();
        }
    }
}
