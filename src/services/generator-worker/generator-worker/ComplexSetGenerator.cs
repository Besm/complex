﻿using Complex.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace Generator
{
    public class ComplexSetGenerator
    {
        public GeneratedChunk Generate(GeneratorTask task)
        {
            List<byte> bytes = new List<byte>();

            for (int y = task.Rect.Position.Y; y < task.Rect.Position.Y + task.Rect.Size.Height; y++)
            {
                for (int x = task.Rect.Position.X; x < task.Rect.Position.X + task.Rect.Size.Width; x++)
                {
                    ComplexNumber c = new ComplexNumber(x / task.ImageSettings.Scale - task.ImageSettings.Offset.X, y / task.ImageSettings.Scale - 1 - task.ImageSettings.Offset.Y);
                    var z = new ComplexNumber();
                    for (int i = 0; i < 400; i++)
                    {
                        z = z * z + c;
                    }

                    if (double.IsInfinity(z.Im) || double.IsInfinity(z.Re) || double.IsNaN(z.Im) || double.IsNaN(z.Re))
                    {
                        bytes.Add(255);
                        bytes.Add(255);
                        bytes.Add(255);
                        bytes.Add(255);
                    }
                    else
                    {
                        bytes.Add(0);
                        bytes.Add(0);
                        bytes.Add(0);
                        bytes.Add(0);
                    }
                }
            }

            string compressedString = CompressBytes(bytes);
            return new GeneratedChunk()
            {
                Rect = task.Rect,
                CompressedBytes = compressedString,
            };
        }

        private static string CompressBytes(List<byte> bytes)
        {
            //  byte[] compressedBytes = Compress(bytes);

            string compressedString = Convert.ToBase64String(bytes.ToArray());
            return compressedString;
        }

        private static byte[] Compress(List<byte> bytes)
        {
            var mem = new MemoryStream();
            var s = new DeflateStream(mem, CompressionLevel.Optimal);
            s.Write(bytes.ToArray(), 0, bytes.Count);
            s.Close();
            var compressedBytes = mem.ToArray();
            return compressedBytes;
        }
    }
}
