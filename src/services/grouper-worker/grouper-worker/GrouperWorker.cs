﻿using Complex.Communication;
using Complex.Grouper.Shared;
using Complex.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Grouper
{
    public class GrouperWorker : ForwardQueueWorker
    {
        private GrouperContext Context { get; set; }
        private Stopwatch watch;
        private ConcurrentQueue<GroupingTask> RecievedTasks = new ConcurrentQueue<GroupingTask>();


        public GrouperWorker(Uri ampqEndpointUri) : base(ampqEndpointUri, "grouping/requests", "images/requests")
        {
            var timer = new Thread(() =>
            {
                watch = new Stopwatch();
                while (true)
                {
                    watch.Restart();

                    if (RecievedTasks.Count != 0)
                    {
                        var tasks = new List<GroupingTask>();

                        while (RecievedTasks.TryDequeue(out GroupingTask task))
                            tasks.Add(task);

                        AddChunksToContext(tasks);
                        Context.SaveChanges();

                        foreach (var imageId in tasks.Select(t => t.ImageId).Distinct())
                        {
                            var task = tasks.First(t => t.ImageId == imageId);
                            GroupIfNeeded(task);
                        }

                        Context.SaveChanges();
                    }

                    var timeToSleep = 250 - (int)watch.ElapsedMilliseconds;

                    if (timeToSleep > 0)
                        Thread.Sleep(timeToSleep);
                }
            });

            Context = new GrouperContext();
            Context.Database.Migrate();

            timer.Start();
        }

        protected override void ProcessMessage(DeliveryEventArgs deliveryEventArgs)
        {
            var task = deliveryEventArgs.GetValue<GroupingTask>();
            RecievedTasks.Enqueue(task);
        }

        private void GroupIfNeeded(GroupingTask task)
        {
            var chunks = Context.Chunks.Where(c => c.ImageGuid == task.ImageId).ToList();
            if (chunks.Count() == task.ChunkCount)
                Group(chunks);
        }

        private void Group(List<Chunk> chunks)
        {
            byte[] imagebytes = StitchChunks(chunks);
            string compressedBytes = CompressBytes(imagebytes);

            var image = new Image
            {
                CompressedBase64RGBABytes = compressedBytes
            };
            Context.SaveChanges();
           // Context.Chunks.RemoveRange(chunks);
           // Context.SaveChanges();
            Publish(image);
        }

        private static byte[] StitchChunks(List<Chunk> chunks)
        {
            var width = chunks.Max(c => c.X + c.Width);
            var height = chunks.Max(c => c.Y + c.Height);
            var imagebytes = new byte[width * height * 4];


            foreach (var chunk in chunks)
            {
                byte[] decompressedChunkBytes = GetDecompressedBytes(chunk);

                int i = 0;
                for (int y = chunk.Y; y < chunk.Y + chunk.Height; y++)
                {
                    for (int x = chunk.X; x < chunk.X + chunk.Width; x++)
                    {
                        int imageIndex = y * width + x;
                        imagebytes[imageIndex] = decompressedChunkBytes[i];
                        i += 4;
                    }
                }
            }

            return imagebytes;
        }

        private void AddChunksToContext(IEnumerable<GroupingTask> tasks)
        {
            var chunks = new List<Chunk>();
            foreach (var task in tasks)
            {
                Chunk chunk = new Chunk()
                {
                    CompressedBytes = task.Chunk.CompressedBytes,
                    X = task.Chunk.Rect.Position.X,
                    Y = task.Chunk.Rect.Position.Y,
                    Width = task.Chunk.Rect.Size.Width,
                    Height = task.Chunk.Rect.Size.Height,
                    ImageGuid = task.ImageId
                };

                chunks.Add(chunk);
            }
            Context.Chunks.AddRange(chunks);

        }

        private string CompressBytes(byte[] bytes)
        {
            var mem = new MemoryStream();
            var deflate = new DeflateStream(mem, CompressionLevel.Optimal);
            deflate.Write(bytes.ToArray(), 0, bytes.Length);
            deflate.Close();
            var compressedBytes = mem.ToArray();

            string compressedString = Convert.ToBase64String(compressedBytes);
            return compressedString;
        }

        private static byte[] GetDecompressedBytes(Chunk chunk)
        {
            var compressedbytes = Convert.FromBase64String(chunk.CompressedBytes);
            /*   var mem = new MemoryStream(compressedbytes);
               var decompressed = new MemoryStream();

               using var deflate = new DeflateStream(mem, CompressionMode.Decompress);

               deflate.CopyTo(decompressed);
               var uncompressedChunkBytes = decompressed.ToArray();
               return uncompressedChunkBytes;
               */
            return compressedbytes;
        }
    }
}
