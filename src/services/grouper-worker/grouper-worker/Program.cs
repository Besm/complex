﻿using System;
using System.Text;

namespace Grouper
{
    class Program
    {
        static void Main(string[] args)
        {
            GrouperWorker worker = new GrouperWorker(new Uri("amqp://rabbitmq.complex-18564455-production.svc.cluster.local"));
            worker.Start();
        }
    }
}
