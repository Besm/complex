﻿using Complex.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace Complex.Api
{
    [ApiController]
    [Route("[controller]")]
    public class GrouperController : ControllerBase
    {
        public HttpClient client;
        public string GrouperApiUrl = "http://complex-grouper-api.complex-18564455-production.svc.cluster.local:3000/";

        public GrouperController(HttpClient client)
        {
            this.client = client;
        }

        [HttpGet]
        public Chunk[] GetChunks(Guid imageId)
        {
            var req = client.GetAsync(GrouperApiUrl + $"chunks?imageId={imageId}");
            string result = req.Result.Content.ReadAsStringAsync().Result;
            Chunk[] enumerable = JsonConvert.DeserializeObject<Chunk[]>(result);
            return enumerable;
        }
    }
}
