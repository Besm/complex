﻿using Complex.Communication;
using Complex.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Complex.Api
{
    [ApiController]
    [Route("[controller]")]
    public class RequestController : ControllerBase
    {

        public MessagePublisher MessagePublisher { get; set; }

        public RequestController(MessagePublisher messagePublisher)
        {
            MessagePublisher = messagePublisher;
        }

        [HttpPost]
        public Guid Generate()
        {
            using var reader = new StreamReader(Request.Body);

            var body = reader.ReadToEndAsync().Result;

            var request = JsonConvert.DeserializeObject<RequestTask>(body);
            var guid = Guid.NewGuid();
            List<IntRectangle> chunkRectangles = DivideInChunkRectangles(request);

            for (int i = 0; i < chunkRectangles.Count; i++)
            {
                IntRectangle rect = chunkRectangles[i];
                var genReq = new GeneratorTask()
                {
                    ImageId = guid,
                    ImageSettings = request.ImageSettings,
                    Rect = rect,
                    ChunkId = i,
                    ChunkCount = chunkRectangles.Count
                };
                MessagePublisher.Publish(genReq);
            }
            return guid;
        }

        private List<IntRectangle> DivideInChunkRectangles(RequestTask request)
        {
            bool isFinished = false;
            var maxChunksize = request.MaxChunkSize;
            IntSize imageSize = request.ImageSettings.Size;
            int x = 0;
            int y = 0;


            if (maxChunksize.Width == 0 || maxChunksize.Height == 0)
            {
                throw new Exception("Max chunk size width and height can't be 0");
            }

            var chunkRectangles = new List<IntRectangle>();

            while (!isFinished)
            {

                var rect = new IntRectangle() { Position = new IntPosition(x, y), Size = maxChunksize };
                IntPosition max = rect.Position + rect.Size;

                if (max.X > imageSize.Width)
                    rect.Size.Width = imageSize.Width - rect.Position.X;

                if (max.Y > imageSize.Height)
                    rect.Size.Height = imageSize.Height - rect.Position.Y;

                chunkRectangles.Add(rect);

                if (x + rect.Size.Width >= imageSize.Width)
                {
                    x = 0;
                    y += maxChunksize.Height;
                }
                else
                {
                    x += maxChunksize.Width;
                }
                isFinished = y >= imageSize.Height;
            }

            return chunkRectangles;
        }
    }
}
