import { RequestTask } from './models/RequestTask';
import { ImageSettings } from './models/ImageSettings';
import { FunctionType } from './models/FunctionType';
import { IntSize } from './models/IntSize';

export class Main {
    constructor() {
    }
    SendRequest() {
        var req = new XMLHttpRequest();
        req.open("POST", "https://localhost:44387/request");
        req.onreadystatechange = function () {
            if (req.readyState === 4) {
                console.log(req.responseText);
            }
        };
        let task = new RequestTask();
        task.ImageSettings = new ImageSettings();
        task.ImageSettings.FunctionType = FunctionType.Mandlebrot100;
        task.ImageSettings.Scale = 1;
        task.ImageSettings.Size = new IntSize(1000, 1000);
        task.MaxChunkSize = new IntSize(200, 200);
        var sss = JSON.stringify(task);
        console.log(sss);
        req.send(sss);
    }
}
