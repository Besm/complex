import { FunctionType } from "./FunctionType";
import { DoublePosition } from "./DoublePosition";
import { IntSize } from "./IntSize";

export class ImageSettings {
    FunctionType: FunctionType;
    Scale: number;
    Offset: DoublePosition;
    Size: IntSize;
    
    constructor() {
    }
}
