export class IntSize {
    Width: number;
    Height: number;
    constructor(width: number, height: number) {
        this.Width = width;
        this.Height = height;
    }
}
