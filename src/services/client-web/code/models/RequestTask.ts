import { IntSize } from "./IntSize";
import { ImageSettings } from "./ImageSettings";

export class RequestTask {
    MaxChunkSize: IntSize;
    ImageSettings: ImageSettings;

    constructor(){

    }
}