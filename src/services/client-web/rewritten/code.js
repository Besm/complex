var guid;


var chunks = [];
var chunksToDraw = [];
var chunkIds = [];
var ctx;

const apiurl = "http://34.78.26.191:2000";
function sendRequest() {
    var stringify = JSON.stringify(GetImageSettings())
    var text = "";
    const response = async () => {
        const r = await fetch(apiurl.concat("/request"),
            {
                method: 'POST',
                mode: 'cors',
                body: stringify
            });
        text = await r.text();
        console.log(r);

        chunkIds = [];
        chunksToDraw = [];
        chunks = [];
        guid = null;
        ctx.fillStyle = 'grey';
        ctx.fillRect(0, 0, document.getElementById("canvas").width, document.getElementById("canvas").height );

        console.log(text);
        guid = text.substr(1, text.length - 2);
    }
    response();


}

function GetNewChunks() {
    fetch(apiurl.concat("/grouper?ImageId=").concat(guid), { mode: 'cors' })
        .then(response => response.json())
        .then(json => {
            var newchunks = json;
            for (let i = 0; i < newchunks.length; i++) {
                const element = newchunks[i];
                if (!chunkIds.includes(element.id)) {
                    chunks.push(element);
                    chunksToDraw.push(element);
                    chunkIds.push(element.id);
                }
            }
        })
}

function getColorIndicesForCoord(x, y, width) {
    var red = y * (width * 4) + x * 4;
    return [red, red + 1, red + 2, red + 3];
}


function DrawNewChunks() {
    chunksToDraw.forEach(chunk => {
//        console.log(chunk);
        if (chunk.imageGuid == guid) {

            var bytes = Uint8ClampedArray.from(atob(chunk.compressedBytes), c => c.charCodeAt(0))
            var x = chunk.x;
            var y = chunk.y - 1;
            var endBytes = new Uint8ClampedArray(bytes.length);
            for (let i = 0; i < bytes.length; i += 4) {

                y++;
                if (y == chunk.height + chunk.y) {
                    x++;
                    y = chunk.y;
                }


                endBytes[i] = 33;
                endBytes[i + 1] = 150;
                endBytes[i + 2] = 243;

                if (bytes[i] == 0) {
                    endBytes[i + 3] = 255;
                }
            }

            var imageData = new ImageData(endBytes, chunk.width, chunk.height);
            ctx.putImageData(imageData, chunk.x, chunk.y);
        }

    });
    chunksToDraw = [];
}

function GetImageSettings() {
    const width = document.getElementById("size_x").value || document.getElementById("size_x").placeholder;
    const height = document.getElementById("size_y").value || document.getElementById("size_y").placeholder;

    document.getElementById("canvas").width = width;
    document.getElementById("canvas").height = height;

    const scale = document.getElementById("scale").value || document.getElementById("scale").placeholder;
    const offset_x = document.getElementById("offset_x").value || document.getElementById("offset_x").placeholder;
    const offset_y = document.getElementById("offset_y").value || document.getElementById("offset_y").placeholder;

    const chunkWidth = document.getElementById("chunkSize_x").value || document.getElementById("chunkSize_x").placeholder;
    const chunkHeight = document.getElementById("chunkSize_y").value || document.getElementById("chunkSize_y").placeholder;


    return {
        ImageSettings: {
            FunctionType: "Mandlebrot100",
            Scale: scale,
            Size: { Width: width, Height: height },
            Offset: { X: offset_x, Y: offset_y }
        },
        MaxChunkSize: { Width: chunkWidth, Height: chunkHeight }
    };
}

function Update() {
    if (guid != null) {
        GetNewChunks();
        DrawNewChunks();
    }
}

function Start() {
    ctx = document.getElementById("canvas").getContext('2d');
    //ctx.fillStyle = 'grey';
    //ctx.fillRect(0,0,600,600);
}

function CallUpdate() {


    setTimeout(() => {
        Update();
        CallUpdate();
    }, 500);
}

Start();
CallUpdate();