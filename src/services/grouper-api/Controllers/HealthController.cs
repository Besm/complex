﻿using Microsoft.AspNetCore.Mvc;

namespace GrouperApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HealthController : ControllerBase
    {
        [HttpGet]
        public bool Health()
        {
            return true;
        }
    }
}
