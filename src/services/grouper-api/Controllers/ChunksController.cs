﻿using System;
using System.Collections.Generic;
using System.Linq;
using Complex.Grouper.Shared;
using Complex.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GrouperApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ChunksController : ControllerBase
    {
        public IGrouperContext Context { get; set; }

        public ChunksController(IGrouperContext context)
        {
            Context = context;
            context.Get().Database.Migrate();
        }

        [HttpGet]
        public IEnumerable<Chunk> GetChunks(Guid imageId)
        {
            if (imageId == Guid.Empty)
                return Context.Get().Chunks.ToList();

            List<Chunk> chunks = Context.Get().Chunks.Where(c => c.ImageGuid == imageId).ToList();
            return chunks;
        }



        [HttpGet("reset")]
        public string DeleteAllChunks()
        {

            int i = Context.Get().Database.ExecuteSqlRaw("TRUNCATE TABLE [Chunks]");

            Context.Get().SaveChanges();
            return $"Removed {i} chunks";
        }
    }
}
